/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Date;



/**
 *
 * @author DogHotel
 */
public class Objednavka
{
    private int id;

    private Date datumOd;

    private Date datumDo;

    private int cena;

    private String jmeno;

    private String prijmeni;

    private String jmenoPes;

    private String rasaPes;

    private velikostPes velikostPes;

    private vekPes vekPes;

    private pohlaviPes pohlaviPes;

    private boolean kastrace;


    public Objednavka(int ID,Date DatumOd, Date DatumDo, int cena,
                      String jmeno, String prijmeni, String jmenoPes,
                      String rasaPes,
                      String velikostPes, String vekPes,
                      String pohlaviPes,
                      int intKastrace)
    {
        this.id = ID;
        this.datumOd = DatumOd;
        this.datumDo = DatumDo;

        this.cena = cena;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.jmenoPes = jmenoPes;
        this.rasaPes = rasaPes;
        this.velikostPes = this.velikostPes.valueOf(velikostPes);

        this.vekPes = this.vekPes.valueOf(vekPes);
        this.pohlaviPes = this.pohlaviPes.valueOf(pohlaviPes);

        if (intKastrace == 1) {
            this.kastrace = true;
        }
        else {
            this.kastrace = false;
        }
    }



    public int vypoctiCenu() //edit later
    {
        //vynulování ceny
        cena = 0;

        LocalDate localDateFrom = datumOd.toLocalDate();
        LocalDate localDateTo   = datumDo.toLocalDate();

        //inicializace hodnot při výpočtu
        int pocetDni = (int) ChronoUnit.DAYS.between(localDateFrom, localDateTo);
        InitializeValues(pocetDni, velikostPes.toString(), vekPes.toString());

        //cena za den pobytu je 50 Kč + extra poplatky, viz předchozí funkce
        cena += pocetDni * 50;

        if (kastrace = false) {
            cena = cena + 250;
        }
        return cena;
    }


    private void InitializeValues(int PocetDni, String velikostPes,
                                  String vekPes)
    {
        switch (velikostPes) {
            case "STREDNI":
                cena += 200 * (PocetDni / 7); //need to cast to int? ( (int)200* ... )
                break;

            case "VELKY":
                cena += 350 * (PocetDni / 7);
                break;

            default:
                break;
        }

        switch (vekPes) {
            case "STENE":
                cena += 50;
                break;

            case "DOSPELY":
                cena += 300;
                break;

            case "SENIOR":
                cena -= 100;
                break;

            default:
                break;
        }
    }


    public void setId(int id)
    {
        this.id = id;
    }


    public int getId()
    {
        return id;
    }


    public Date getDatumOd()
    {
        return datumOd;
    }


    public void setDatumOd(Date datumOd)
    {
        this.datumOd = datumOd;
    }


    public Date getDatumDo()
    {
        return datumDo;
    }


    public void setDatumDo(Date datumDo)
    {
        this.datumDo = datumDo;
    }


    public int getCena()
    {
        return cena;
    }


    public void setCena(int cena)
    {
        this.cena = cena;
    }


    public String getJmeno()
    {
        return jmeno;
    }


    public void setJmeno(String jmeno)
    {
        this.jmeno = jmeno;
    }


    public String getPrijmeni()
    {
        return prijmeni;
    }


    public void setPrijmeni(String prijmeni)
    {
        this.prijmeni = prijmeni;
    }


    public String getJmenoPes()
    {
        return jmenoPes;
    }


    public void setJmenoPes(String jmenoPes)
    {
        this.jmenoPes = jmenoPes;
    }


    public String getRasaPes()
    {
        return rasaPes;
    }


    public void setRasaPes(String rasaPes)
    {
        this.rasaPes = rasaPes;
    }


    public velikostPes getVelikostPes()
    {
        return velikostPes;
    }


    public void setVelikostPes(velikostPes velikostPes)
    {
        this.velikostPes = velikostPes;
    }


    public vekPes getVekPes()
    {
        return vekPes;
    }


    public void setVekPes(vekPes vekPes)
    {
        this.vekPes = vekPes;
    }


    public pohlaviPes getPohlaviPes()
    {
        return pohlaviPes;
    }


    public void setPohlaviPes(pohlaviPes pohlaviPes)
    {
        this.pohlaviPes = pohlaviPes;
    }


    public boolean isKastrace()
    {
        return kastrace;
    }


    public void setKastrace(boolean kastrace)
    {
        this.kastrace = kastrace;
    }


}
