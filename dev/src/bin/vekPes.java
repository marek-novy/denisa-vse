/* The file is saved in UTF-8 codepage.
 * Check: «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package bin;



/*******************************************************************************
 * Instance třídy {@code vekPes} představují ...
 * Instances of class {@code vekPes} represent ...
 *

 */

    public enum vekPes
    {
        STENE,
        DOSPLELY,
        SENIOR;

    }