/* The file is saved in UTF-8 codepage.
 * Check: «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package bin;



/*******************************************************************************
 * Instance třídy {@code velikostPes} představují ...
 * Instances of class {@code velikostPes} represent ...

 */
    public enum velikostPes
    {
        MALY,
        STREDNI,
        VELKY;

    }