/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bin;

import GUI.ScrCreateOrderController;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.layout.BorderImage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javafx.collections.FXCollections.*;



/**
 * Repozitory class. Pracuje s databazi a poskytuje z ni data.
 *
 * @author Stanislav Chitov, Lucie Marková, Denisa Nová
 * @version 2019-SUMMER
 */
public class Hotel
{
    private int kapacita;


    private static String napoveda;

    private int i;

    private static Connection connection;

    private PreparedStatement preparedStatement;

    // singleton
    private static Hotel singleton;

    private Hotel()
    {
//        seznamObjednavek = new HashMap<>();
        kapacita = 0;
        i = 0;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:Objednavky");
        }
        catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).log(Level.SEVERE, "Cannot create db connection.", ex);
        }

        // vytvori DB table if doesnt exist
        vytvorDBTable();
        // nacte objednavky z DB do seznamObjednavek
        nactiSeznamObjednavek();
        //              ulozeni testovacich prvnich dat

    }

    /**
     * Vrátí instanci repozitáře.
     *
     */
    static public Hotel getinstance()
    {
        if (singleton == null) {
            singleton = new Hotel();
        }
        return singleton;
    }
    ;

    ;

    public static String getNapoveda()
    {
        return napoveda;
    }


    public int getKapacita()
    {
        return kapacita;
    }


    public void setKapacita(int kapacita)
    {
        this.kapacita = kapacita;
    }


    private void vytvorDBTable()
    {
        ArrayList<Objednavka> arrayObjednavky = new ArrayList<Objednavka>();
        try {
            Statement statement;
            statement = connection.createStatement();
            statement.executeUpdate("drop table if exists objednavky");
            statement.executeUpdate(
                    "create table if not exists objednavky(objednavkaID INTEGER PRIMARY KEY AUTOINCREMENT,datumOd date,datumDo date,cena INTEGER ,jmeno varchar(50),prijmeni varchar(50),jmenoPes varchar(50),rasaPes varchar(50),velikostPes varchar(50),vekPes varchar(50),pohlaviPes varchar(50),kastrace bit);");

////              ulozeni testovacich prvnich dat
//        arrayObjednavky.add(
//                    new Objednavka( Date.valueOf("2019-12-12"), Date.valueOf(
//                                   "2020-02-11"), 250, "Marek", "Novy", "Alik",
//                                   "civava", "MALY", "STENE", "PES", 0));
//        arrayObjednavky.add(
//                    new Objednavka( Date.valueOf("2020-07-27"), Date.valueOf(
//                                   "2020-08-23"), 3500, "Radek", "Druhy", "Chio",
//                                   "ratlik", "MALY", "SENIOR", "FENA", 1));

                        for (Objednavka objednavka : arrayObjednavky) {
            preparedStatement = connection.prepareStatement(
                    "insert into objednavky (datumOd,datumDO,cena,jmeno,prijmeni,jmenoPes,rasaPes,velikostPes,vekPes,pohlaviPes,kastrace) values(?,?,?,?,?,?,?,?,?,?,?);");
//            preparedStatement.setInt(1, objednavka.getId());
            preparedStatement.setDate(1, objednavka.getDatumOd());
            preparedStatement.setDate(2, objednavka.getDatumDo());
            preparedStatement.setInt(3, objednavka.getCena());
            preparedStatement.setString(4, objednavka.getJmeno());
            preparedStatement.setString(5, objednavka.getPrijmeni());
            preparedStatement.setString(6, objednavka.getJmenoPes());
            preparedStatement.setString(7, objednavka.getRasaPes());
            velikostPes velikost = objednavka.getVelikostPes();
            String sVelikost = velikost.toString();
            preparedStatement.setString(8, sVelikost);

            vekPes vek = objednavka.getVekPes();
            String sVek = vek.toString();
            preparedStatement.setString(9, sVek);

            pohlaviPes pohlavi = objednavka.getPohlaviPes();
            String sPohlavi = pohlavi.toString();
            preparedStatement.setString(10, sPohlavi);

                Boolean kastrace = objednavka.isKastrace();
                int intKastrace = 0;
                if (kastrace) {
                    intKastrace = 1;
                }
            preparedStatement.setInt(11, intKastrace);

            preparedStatement.executeUpdate();

//            //add to seznamObjednave
//            seznamObjednavek.add(objednavka);
                        }
        }
        catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

public List<Objednavka> filtrujDleDatumy(Date datumOdf, Date DatumDof){
    ArrayList<Objednavka> result = new ArrayList<Objednavka>();
    if (datumOdf != null && DatumDof != null)
        {
            System.out.print("Podminka 1");
        String statement = "SELECT * FROM objednavky WHERE datumOd >= ? AND datumDo <= ?";
        Objednavka fObjednavka = null;

        try {
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setDate(1, datumOdf);
            preparedStatement.setDate(2, DatumDof);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int objednavkaID = resultSet.getInt("objednavkaID");
                Date datumOd = resultSet.getDate("datumOd");
                Date datumDo = resultSet.getDate("datumDo");
                int cena = resultSet.getInt("cena");
                String jmeno = resultSet.getString("jmeno");
                String prijmeni = resultSet.getString("prijmeni");
                String jmenoPes = resultSet.getString("jmenoPes");
                String rasaPes = resultSet.getString("rasaPes");
                String velikostPes = resultSet.getString("velikostPes");
                String vekPes = resultSet.getString("vekPes");
                String pohlaviPes = resultSet.getString("pohlaviPes");
                int kastrace = resultSet.getByte("kastrace");

            fObjednavka  = new Objednavka(objednavkaID, datumOd, datumDo, cena,
                                         jmeno,
                                         prijmeni, jmenoPes, rasaPes,
                                         velikostPes,
                                         vekPes, pohlaviPes, kastrace);
            result.add(fObjednavka);
            }

        }
        catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);
        }}
        if (datumOdf != null && DatumDof == null){
            System.out.print("Podminka 2");
            String statement = "SELECT * FROM objednavky WHERE datumOd >= ?";
        Objednavka fObjednavka = null;

        try {
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setDate(1, datumOdf);
//            preparedStatement.setDate(2, DatumDof);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int objednavkaID = resultSet.getInt("objednavkaID");
                Date datumOd = resultSet.getDate("datumOd");
                Date datumDo = resultSet.getDate("datumDo");
                int cena = resultSet.getInt("cena");
                String jmeno = resultSet.getString("jmeno");
                String prijmeni = resultSet.getString("prijmeni");
                String jmenoPes = resultSet.getString("jmenoPes");
                String rasaPes = resultSet.getString("rasaPes");
                String velikostPes = resultSet.getString("velikostPes");
                String vekPes = resultSet.getString("vekPes");
                String pohlaviPes = resultSet.getString("pohlaviPes");
                int kastrace = resultSet.getByte("kastrace");

            fObjednavka  = new Objednavka(objednavkaID, datumOd, datumDo, cena,
                                         jmeno,
                                         prijmeni, jmenoPes, rasaPes,
                                         velikostPes,
                                         vekPes, pohlaviPes, kastrace);
            result.add(fObjednavka);
            }}
            catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);       }}

        if (datumOdf == null && DatumDof != null){
            System.out.print("Podminka 3");
            String statement = "SELECT * FROM objednavky WHERE datumDo <= ?";
        Objednavka fObjednavka = null;

        try {
            preparedStatement = connection.prepareStatement(statement);
//            preparedStatement.setDate(1, datumOdf);
            preparedStatement.setDate(1, DatumDof);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int objednavkaID = resultSet.getInt("objednavkaID");
                Date datumOd = resultSet.getDate("datumOd");
                Date datumDo = resultSet.getDate("datumDo");
                int cena = resultSet.getInt("cena");
                String jmeno = resultSet.getString("jmeno");
                String prijmeni = resultSet.getString("prijmeni");
                String jmenoPes = resultSet.getString("jmenoPes");
                String rasaPes = resultSet.getString("rasaPes");
                String velikostPes = resultSet.getString("velikostPes");
                String vekPes = resultSet.getString("vekPes");
                String pohlaviPes = resultSet.getString("pohlaviPes");
                int kastrace = resultSet.getByte("kastrace");

            fObjednavka  = new Objednavka( objednavkaID,datumOd, datumDo, cena,
                                         jmeno,
                                         prijmeni, jmenoPes, rasaPes,
                                         velikostPes,
                                         vekPes, pohlaviPes, kastrace);
            result.add(fObjednavka);
            }}
            catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);       }}

//        seznamObjednavek.notifyAll();
        return result;

    }

    public void updateObjednavka(Objednavka objednavka){
    String sql = "UPDATE objednavky SET datumOd = ?, datumDo = ?, cena = ?, jmeno = ?, prijmeni = ?, jmenoPes = ?, rasaPes = ?, velikostPes = ?, vekPes = ?, pohlaviPes = ?, kastrace = ? WHERE objednavkaID = ?;";
    try {

    preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDate(1, objednavka.getDatumOd());
            preparedStatement.setDate(2, objednavka.getDatumDo());
            preparedStatement.setInt(3, objednavka.getCena());
            preparedStatement.setString(4, objednavka.getJmeno());
            preparedStatement.setString(5, objednavka.getPrijmeni());
            preparedStatement.setString(6, objednavka.getJmenoPes());
            preparedStatement.setString(7, objednavka.getRasaPes());
            preparedStatement.setString(8, objednavka.getVelikostPes().toString());
            preparedStatement.setString(9, objednavka.getVekPes().toString());
            preparedStatement.setString(10, objednavka.getPohlaviPes().toString());
            int intKastrace = 0;
            if (objednavka.isKastrace()) {
                intKastrace = 1;
            }
            preparedStatement.setInt(11, intKastrace);

            preparedStatement.setInt(12, objednavka.getId());
            preparedStatement.executeUpdate();
    }
        catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

    public List<Objednavka> nactiSeznamObjednavek()
    {

        ArrayList<Objednavka> arrayObjednavek = new ArrayList<Objednavka>();
        try {
            ResultSet rs = connection.createStatement()
                    .executeQuery("select * from objednavky");
            while (rs.next()) {
                arrayObjednavek.add(new Objednavka(rs.getInt("ObjednavkaID"),
                                                    rs.getDate("datumOd"), rs.
                                                    getDate("datumDo"), rs.
                                                    getInt("cena"), rs.
                                                    getString("jmeno"),
                                                    rs.getString("prijmeni"),
                                                    rs.getString("jmenoPes"),
                                                    rs.getString("rasaPes"), rs.
                                                    getString("velikostPes"),
                                                    rs.getString("vekPes"), rs.
                                                    getString("pohlaviPes"), rs.
                                                    getInt("kastrace")));
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        return arrayObjednavek;
    }

    /**
     * Prida do objednavku do databaze a ulozi objednavku do listu seznamObjednavek.
     *
     * @param stage hahahahahahaha
     * @throws java.lang.Exception Obecná instance třídy Exception
     */
    public Objednavka pridejObjednavku(Objednavka objednavka)
    {
//        //konvertujeme local date na sql.date
//        LocalDate ldDateDo = order.getDatePickerTo();
//        Date dateDo = Date.valueOf(ldDateDo);
//        LocalDate ldDateOd = order.getDatePickerFrom();
//        Date dateOd = Date.valueOf(ldDateOd);
//        i = i + 1;
//        int intKastrace = 0;
//        if (order.getCheckBoxCastrated()) {
//            intKastrace = 1;
//        }
//        Objednavka objednavka = new Objednavka(i, dateOd, dateDo, order.
//                                               getTextFieldPrice(),
//                                               order.getTextFieldOwnerName(),
//                                               order.getTextFieldOwnerSurname(),
//                                               order.getTextFieldDogName(),
//                                               order.getTextFieldDogRace().
//                                                       toString(), order.
//                                                       getComboBoxDogSize().
//                                                       toString(), order.
//                                                       getComboBoxDogAge().
//                                                       toString(),
//                                               order.getComboBoxDogSex().
//                                                       toString(), intKastrace);

//save to the DB
        try {
            preparedStatement = connection.prepareStatement(
                    "insert into objednavky (datumOd,datumDO,cena,jmeno,prijmeni,jmenoPes,rasaPes,velikostPes,vekPes,pohlaviPes,kastrace) values(?,?,?,?,?,?,?,?,?,?,?);");
            preparedStatement.setDate(1, objednavka.getDatumOd());
            preparedStatement.setDate(2, objednavka.getDatumDo());
            preparedStatement.setInt(3, objednavka.getCena());
            preparedStatement.setString(4, objednavka.getJmeno());
            preparedStatement.setString(5, objednavka.getPrijmeni());
            preparedStatement.setString(6, objednavka.getJmenoPes());
            preparedStatement.setString(7, objednavka.getRasaPes());
            velikostPes velikost = objednavka.getVelikostPes();
            String sVelikost = velikost.toString();
            preparedStatement.setString(8, sVelikost);

            vekPes vek = objednavka.getVekPes();
            String sVek = vek.toString();
            preparedStatement.setString(9, sVek);

            pohlaviPes pohlavi = objednavka.getPohlaviPes();
            String sPohlavi = pohlavi.toString();
            preparedStatement.setString(10, sPohlavi);

                Boolean kastrace = objednavka.isKastrace();
                int intKastrace = 0;
                if (kastrace) {
                    intKastrace = 1;
                }
            preparedStatement.setInt(11, intKastrace);

            preparedStatement.executeUpdate();

        }
        catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);
        }

return objednavka;
    }


    public void odeberObjednavku(int id)
    {
        String sql = "DELETE FROM objednavky WHERE objednavkaID = ?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            Objednavka objednavka = najdiObjednavku(id);

        }
        catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);
        }

    }


    public Objednavka najdiObjednavku(int id)
    {
        String statement = "SELECT * FROM objednavky WHERE objednavkaID = ?";
        Objednavka obj = null;
        try {
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int objednavkaID = resultSet.getInt("objednavkaID");
                Date datumOd = resultSet.getDate("datumOd");
                Date datumDo = resultSet.getDate("datumDo");
                int cena = resultSet.getInt("cena");
                String jmeno = resultSet.getString("jmeno");
                String prijmeni = resultSet.getString("prijmeni");
                String jmenoPes = resultSet.getString("jmenoPes");
                String rasaPes = resultSet.getString("rasaPes");
                String velikostPes = resultSet.getString("velikostPes");
                String vekPes = resultSet.getString("vekPes");
                String pohlaviPes = resultSet.getString("pohlaviPes");
                int kastrace = resultSet.getByte("kastrace");

                obj
                        = new Objednavka(objednavkaID, datumOd, datumDo, cena,
                                         jmeno,
                                         prijmeni, jmenoPes, rasaPes,
                                         velikostPes,
                                         vekPes, pohlaviPes, kastrace);

                // Timestamp -> LocalDateTime
                return obj;
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(Hotel.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        return obj;
    }

//
//    public boolean existujeObjednavka(int id)
//    {
//        Objednavka objednavka = najdiObjednavku(id);
//        return seznamObjednavek.contains(objednavka);
//    }

}


