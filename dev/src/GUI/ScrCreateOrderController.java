package GUI;

import static GUI.Main.parentWindow;
import static GUI.Main.previousWindow;
import bin.pohlaviPes;
import bin.vekPes;
import bin.velikostPes;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 * Controller třída pro obrazovku CreateOrder.
 *
 * @author Stanislav Chitov, Lucie Marková, Denisa Nová
 * @version 2019-SUMMER
 */
public class ScrCreateOrderController implements Initializable {

    /** Inicializace všech komponentů UI ze souboru FXML. */
    @FXML
    private DatePicker datePickerFrom;
    @FXML
    private DatePicker datePickerTo;
    @FXML
    private Button btnAddOrder;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField textFieldOwnerName;
    @FXML
    private TextField textFieldDogName;
    @FXML
    private TextField textFieldDogRace;
    @FXML
    private TextField textFieldOwnerSurname;
    @FXML
    private TextField textFieldAdditionalInfo;
    @FXML
    private ComboBox<pohlaviPes> comboBoxDogSex;
    @FXML
    private ComboBox<vekPes> comboBoxDogAge;
    @FXML
    private TextField textFieldPrice;
    @FXML
    private CheckBox checkBoxCastrated;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ComboBox<velikostPes> comboBoxDogSize;
    @FXML
    private VBox vBoxScrCreateOrder;

    public LocalDate getDatePickerFrom() {
        return datePickerFrom.getValue();
    }

    public LocalDate getDatePickerTo() {
        return datePickerTo.getValue();
    }

    public String getTextFieldOwnerName() {
        return textFieldOwnerName.getText();
    }

    public String getTextFieldDogName() {
        return textFieldDogName.getText();
    }

    public String getTextFieldDogRace() {
        return textFieldDogRace.getText();
    }

    public String getTextFieldOwnerSurname() {
        return textFieldOwnerSurname.getText();
    }

    public String getTextFieldAdditionalInfo() {
        return textFieldAdditionalInfo.getText();
    }

    public pohlaviPes getComboBoxDogSex() {
        return comboBoxDogSex.getSelectionModel().getSelectedItem();
    }

    public vekPes getComboBoxDogAge() {
        return comboBoxDogAge.getSelectionModel().getSelectedItem();
    }

    public int getTextFieldPrice() {
        return Integer.parseInt(textFieldPrice.getText());
    }

    public boolean getCheckBoxCastrated() {
        return checkBoxCastrated.isSelected();
    }

    public velikostPes getComboBoxDogSize() {
        return comboBoxDogSize.getSelectionModel().getSelectedItem();
    }

    /**
     * Metoda inicializující třídu controlleru.
     *
     * @param url instance URL
     * @param rb  instance ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /** přiřadí previousWindow stringovou hodnotu jména předchozího okna. */
        previousWindow = parentWindow.getScene().getRoot().toString();
        setData();
    }

    private void setData(){
        //vyčištění comboboxů
        comboBoxDogAge.getItems().clear();
        comboBoxDogSex.getItems().clear();
        comboBoxDogSize.getItems().clear();

        //nahrání enumů do comboboxů
        comboBoxDogAge.getItems().setAll(vekPes.values());
        comboBoxDogSex.getItems().setAll(pohlaviPes.values());
        comboBoxDogSize.getItems().setAll(velikostPes.values());

//        comboBoxDogAge.getSelectionModel().select(objednavka.dogAge); - něco v takovémhle tvaru - pro edit a order details
//          - stejně to nastavení asi bude pro všechny FXML prvky, které se budou brát z databáze
    }

    /**
     * Metoda, která uživatele při pokusu o zavření okna pomocí alertu upozorní,
     * že provedené změny doposud nebyly uloženy.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void alertOnUnsaved(ActionEvent event) throws IOException {

        //vytvoření instance třídy Alert
        Alert alertUnsaved = new Alert(Alert.AlertType.WARNING,
            "Chcete zahodit neuložené změny?",
            ButtonType.YES, ButtonType.NO);
        alertUnsaved.setTitle("Varování");
        alertUnsaved.setHeaderText("Neuložené změny");

        //ošetření NullPointerException pomocí třídy Optional
        Optional<ButtonType> result = alertUnsaved.showAndWait();

        //pokud uživatel zvolí tlačítko ano, bude načtena původní obrazovka
        if (result.get() == ButtonType.YES)
            loadPrevScr(event);
    }

    /**
     * Metoda informující uživatele o úspěšném uložení provedených změn.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void alertSaved(ActionEvent event) throws IOException {

        //vytvoření instance třídy Alert
        Alert alertFinished = new Alert(Alert.AlertType.INFORMATION);

        alertFinished.setTitle("Operace provedena");
        alertFinished.setHeaderText("");
        alertFinished.setContentText("Změny byly úspěšně uloženy.");
        alertFinished.showAndWait();

        //načtení původní obrazovky
        loadPrevScr(event);
    }

    /**
     * Metoda sloužící k načtení původní obrazovky.
     *
     * @param event event
     * @throws IOException
     */
    private void loadPrevScr(ActionEvent event) throws IOException {

        //ověří, zda se předchozí obrazovka jménem rovná obrazovce Main
        if (previousWindow.equals("VBox[id=vBoxScrMain, styleClass=root]"))
            //načte scrMain.fxml a následně nastaví scénu na tuto obrazovku
            Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                .getResource("scrMain.fxml")));

        else
            //načte scrOrder.fxml a následně nastaví scénu na tuto obrazovku
            Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                .getResource("scrOrder.fxml")));
    }

//    @FXML
//    private int CalculatePrice()
//    {
//        return vypoctiCenu(getDatePickerFrom(), getDatePickerTo(),
//                getComboBoxDogSize(), getComboBoxDogAge());
//    }
}
