package GUI;

import bin.Hotel;
import bin.Objednavka;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import static GUI.Main.parentWindow;
import static GUI.Main.previousWindow;

/**
 * Controller třída pro obrazovku Order.
 *
 * @author Stanislav Chitov, Lucie Marková, Denisa Nová
 * @version 2019-SUMMER
 */
public class ScrOrderController implements Initializable {

    /**
     * Inicializace všech komponentů UI ze souboru FXML.
     */
    @FXML
    private MenuItem menuItemCreateOrder;
    @FXML
    private MenuItem menuItemEnd;
    @FXML
    private MenuItem menuItemAbout;
    @FXML
    private MenuItem menuItemHelp;
    @FXML
    private ListView<Objednavka> listViewOrders;
    @FXML
    private DatePicker datePickerFrom;
    @FXML
    private DatePicker datePickerTo;
    @FXML
    private TextField textFieldSearch;
    @FXML
    private Button btnApplyFilter;
    @FXML
    private Button btnCancelFilter;
    @FXML
    private Button btnDelete;
    @FXML
    private Button btnAddOrder;
    @FXML
    private Button btnShowDetails;
    @FXML
    private MenuItem contextMenuDetaily;
    @FXML
    private MenuItem contextMenuEditovat;
    @FXML
    private VBox scrOrder;

    private Hotel hotel;

    private ObservableList<Objednavka> observableObjednavky;

    /**
     * Metoxa inicializující třídu controlleru.
     *
     * @param url instance URL
     * @param rb  instance ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        hotel = Hotel.getinstance();
        observableObjednavky = FXCollections.observableArrayList();
        observableObjednavky.addAll(hotel.nactiSeznamObjednavek());
        /* nastaví, že tlačítka Editovat objednávku a Zobrazit detaily jsou
           neaktivní, pokud není zvolená objednávka z listu objednávek. */
        btnDelete.disableProperty().bind(listViewOrders.getSelectionModel()
                                                       .selectedItemProperty().isNull());
        btnShowDetails.disableProperty().bind(listViewOrders.getSelectionModel()
                                                            .selectedItemProperty().isNull());

        listViewOrders.setItems(observableObjednavky);
        listViewOrders.setCellFactory(new Callback<ListView<Objednavka>, ListCell<Objednavka>>() {
            @Override
            public ListCell<Objednavka> call(ListView<Objednavka> param) {
                ListCell<Objednavka> cell = new ListCell<Objednavka>() {
                    @Override
                    protected void updateItem(Objednavka item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null && !empty) {
                            setText(item.getId() + " - " + item.getJmenoPes() + " - " + item.getJmeno() + " " + item
                                    .getPrijmeni() + " | " + item.getDatumOd() + " do " + item.getDatumDo());
                        } else {
                            setText("");
                        }
                    }
                };
                return cell;
            }
        });

        //////////////////////////
        // Filter
        btnApplyFilter.setOnMouseClicked(event -> {
            LocalDate fromLocalDate = datePickerFrom.getValue();
            LocalDate toLocalDate = datePickerTo.getValue();

            Date fromDate = null;
            Date toDate = null;
            if (fromLocalDate != null) {
                fromDate = Date.valueOf(fromLocalDate);
            }
            if (toLocalDate != null) {
                toDate = Date.valueOf(toLocalDate);
            }
            observableObjednavky.clear();
            observableObjednavky.addAll(Hotel.getinstance().filtrujDleDatumy(fromDate, toDate));
            listViewOrders.refresh();

        });
        btnCancelFilter.setOnMouseClicked(event ->
                                          {
                                              observableObjednavky.clear();
                                              observableObjednavky.addAll(Hotel.getinstance().nactiSeznamObjednavek());
                                          });

        btnDelete.setOnMouseClicked(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Opravdu chcete záznam smazat?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                Objednavka objednavka = listViewOrders.getSelectionModel().getSelectedItem();
                if (objednavka != null) {
                    Hotel.getinstance().odeberObjednavku(objednavka.getId());
                }
                observableObjednavky.remove(objednavka);
            }
        });

        /** přiřadí previousWindow stringovou hodnotu jména předchozího okna. */
        previousWindow = parentWindow.getScene().getRoot().toString();

    }

    /**
     * Metoda sloužící k načtení obrazovky Order.
     *
     * @param event event
     * @throws IOException
     */
    private void loadScrOrders(ActionEvent event) throws IOException {
        //načte scrOrder.fxml a následně nastaví scénu na tuto obrazovku

        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                                                                     .getResource("scrOrder.fxml")));
    }

    /**
     * Pomocná metoda sloužící k načtení obrazovky CreateOrder, která se spustí
     * pouze v případě použití klávesové zkratky (jedná se o položku menu).
     *
     * @param event
     * @throws IOException
     */
    @FXML
    private void menuScrCreateOrder(ActionEvent event) throws IOException {
        //načte scrCreateOrder.fxml a následně nastaví scénu na tuto obrazovku
        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                                                                     .getResource("scrOrderDetails.fxml")));
    }

    /**
     * Metoda sloužící k načtení obrazovky EditOrder.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadScrEditOrder(ActionEvent event) throws IOException {
        //načte scrEditOrder.fxml a následně nastaví scénu na tuto obrazovku

        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass().getResource("scrEditOrder.fxml")));
    }

    /**
     * Metoda sloužící k načtení obrazovky OrderDetails.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadScrOrderDetails(ActionEvent event) throws IOException {
        //načte scrOrderDetails.fxml a následně nastaví scénu na tuto obrazovku
        Main.setCurrentObjednavka(listViewOrders.getSelectionModel().getSelectedItem());
        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                                                                     .getResource("scrOrderDetails.fxml")));
    }

    /**
     * Metoda sloužící k ukončení aplikace.
     *
     * @param event
     */
    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metoda sloužící k zobrazení informací o programu prostřednictvím Alertu.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadAbout(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("O programu");
        alert.setHeaderText("Semestrální práce - Dog Hotel");
        alert.setContentText("verze LS 2019");
        alert.showAndWait();
    }

    /**
     * Metoda sloužící k načtení obrazovky Help.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadScrHelp(ActionEvent event) throws IOException {
        //načte scrHelp.fxml a následně nastaví scénu na tuto obrazovku
        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass().getResource("scrHelp.fxml")));
    }

}
