package GUI;

import static GUI.Main.parentWindow;
import static GUI.Main.previousWindow;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * Controller třída pro obrazovku Help.
 *
 * @author Stanislav Chitov, Lucie Marková, Denisa Nová
 * @version 2019-SUMMER
 */
public class ScrHelpController implements Initializable {

    /** Inicializace všech komponentů UI ze souboru FXML. */
    @FXML
    private Button btnReturn;
    @FXML
    private TextField textFieldHelp;
    @FXML
    private VBox vBoxScrHelp;

    /**
     * Metoxa inicializující třídu controlleru.
     * 
     * @param url instance URL
     * @param rb  instance ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /** přiřadí previousWindow stringovou hodnotu jména předchozího okna. */
        previousWindow = parentWindow.getScene().getRoot().toString();
    }    
    
    /**
     * Metoda sloužící k načtení původní obrazovky.
     * 
     * @param event event
     * @throws IOException 
     */
    @FXML
    private void loadPrevScr(ActionEvent event) throws IOException {
        
        //ověří, zda se předchozí obrazovka jménem rovná obrazovce Main
        if (previousWindow.equals("VBox[id=vBoxScrMain, styleClass=root]"))
            //načte scrMain.fxml a následně nastaví scénu na tuto obrazovku
            Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                .getResource("scrMain.fxml")));
        
        /*previousWindow se nerovná scrMain -> rovná se scrOrder 
                -> načte scrOrder */
        else 
            //načte scrOrder.fxml a následně nastaví scénu na tuto obrazovku
            Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                .getResource("scrOrder.fxml")));
    }
    
}
