package GUI;

import bin.Hotel;
import bin.Objednavka;
import bin.pohlaviPes;
import bin.vekPes;
import bin.velikostPes;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ResourceBundle;

import static GUI.Main.parentWindow;
import static GUI.Main.previousWindow;

/**
 * Controller třída pro obrazovku OrderDetails.
 *
 * @author Stanislav Chitov, Lucie Marková, Denisa Nová
 * @version 2019-SUMMER
 */
public class ScrOrderDetailsController implements Initializable {

    /**
     * Inicializace všech komponentů UI ze souboru FXML.
     */
    @FXML
    private DatePicker datePickerFrom;
    @FXML
    private DatePicker datePickerTo;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField textFieldOwnerName;
    @FXML
    private TextField textFieldDogName;
    @FXML
    private TextField textFieldDogRace;
    @FXML
    private TextField textFieldOwnerSurname;
    @FXML
    private TextField textFieldAdditionalInfo;
    @FXML
    private ComboBox<pohlaviPes> comboBoxDogSex;
    @FXML
    private ComboBox<vekPes> comboBoxDogAge;
    @FXML
    private TextField textFieldPrice;
    @FXML
    private CheckBox checkBoxCastrated;
    @FXML
    private VBox vBox;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ComboBox<velikostPes> comboBoxDogSize;
    private Objednavka objednavka;
    private int cena = 0;

    /**
     * Metoxa inicializující třídu controlleru.
     *
     * @param url instance URL
     * @param rb  instance ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /** přiřadí previousWindow stringovou hodnotu jména předchozího okna. */
        previousWindow = parentWindow.getScene().getRoot().toString();
        objednavka = Main.getCurrentObjednavka();
        Main.setCurrentObjednavka(null);

        //nahrání enumů do comboboxů
        comboBoxDogAge.getItems().setAll(vekPes.values());
        comboBoxDogSex.getItems().setAll(pohlaviPes.values());
        comboBoxDogSize.getItems().setAll(velikostPes.values());

        if (objednavka != null) {

            textFieldOwnerName.setText(objednavka.getJmeno());
            textFieldOwnerSurname.setText(objednavka.getPrijmeni());
            textFieldDogName.setText(objednavka.getJmenoPes());
            textFieldDogRace.setText(objednavka.getRasaPes());
            textFieldPrice.setText(String.valueOf(objednavka.getCena()));

            checkBoxCastrated.setSelected(objednavka.isKastrace());
            comboBoxDogSex.setValue(objednavka.getPohlaviPes());
            comboBoxDogAge.setValue(objednavka.getVekPes());
            comboBoxDogSize.setValue(objednavka.getVelikostPes());
            datePickerFrom.setValue(objednavka.getDatumOd().toLocalDate());
//                    .toInstant()
//                                              .atZone(ZoneId.systemDefault())
//                                              .toLocalDate());
            datePickerTo.setValue(objednavka.getDatumDo().toLocalDate());
//                    .toInstant()
//                                            .atZone(ZoneId.systemDefault())
//                                            .toLocalDate());
        }

        /** save on click **/
        btnSave.setOnAction(event -> {

            if(objednavka == null){
                // pridej
                //
                   objednavka = this.vydejObjednavku();
                if (objednavka.getDatumOd() == null || objednavka.getDatumDo() == null || objednavka.getJmeno() == null || objednavka.getJmenoPes() == null || objednavka.getPrijmeni()== null|| objednavka.getPohlaviPes()== null || objednavka.getRasaPes()== null || objednavka.getVelikostPes()== null || objednavka.getVekPes()== null ) {
                    Alert alertMandatory = new Alert(Alert.AlertType.INFORMATION);

            alertMandatory.setTitle("Nejsou vyplněná všechna povinná pole.");
            alertMandatory.setHeaderText("");
            alertMandatory.setContentText("Prosím, vyplňte všechna povinná pole.");
            alertMandatory.showAndWait();
                } else {

                Hotel.getinstance().pridejObjednavku(objednavka);
                Alert alertFinished = new Alert(Alert.AlertType.INFORMATION);

            alertFinished.setTitle("Operace provedena");
            alertFinished.setHeaderText("");
            alertFinished.setContentText("Změny byly úspěšně uloženy.");
            alertFinished.showAndWait();

            }}
            else{
                System.out.println("UPDATE");
                Objednavka formObjednavka = this.vydejObjednavku();
                Hotel.getinstance().updateObjednavka(formObjednavka);

                Alert alertFinished = new Alert(Alert.AlertType.INFORMATION);
            alertFinished.setTitle("Operace provedena");
            alertFinished.setHeaderText("");
            alertFinished.setContentText("Změny byly úspěšně uloženy.");
            alertFinished.showAndWait();

                try {
                Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                                                                             .getResource("scrOrder.fxml")));
            } catch (IOException e) {
                e.printStackTrace();}
            }




            try {
                Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                                                                             .getResource("scrOrder.fxml")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });}
    /**
     *
     * @return
     */
    public Objednavka vydejObjednavku()
    {int objCena = 0;
        LocalDate ldDateDo = this.getDatePickerTo();
        LocalDate ldDateOd = this.getDatePickerFrom();
if (ldDateOd == null || ldDateDo == null || this.getTextFieldOwnerName() == null || this.getTextFieldDogName() == null || this.getTextFieldOwnerSurname()== null|| this.getComboBoxDogSex()== null || this.getTextFieldDogRace()== null || this.getComboBoxDogSize()== null || this.getComboBoxDogAge()== null ) {
                    Alert alertMandatory = new Alert(Alert.AlertType.INFORMATION);

            alertMandatory.setTitle("Nejsou vyplněná všechna povinná pole.");
            alertMandatory.setHeaderText("");
            alertMandatory.setContentText("Prosím, vyplňte všechna povinná pole.");
            alertMandatory.showAndWait();
                }
String strCena = this.getTextFieldPrice();
if (strCena == null) {
    vypoctiCenu();
    objCena = cena;
}    else {
    objCena = Integer.parseInt(strCena);
            }
int ID = 0;
if (objednavka != null) {
    ID = objednavka.getId();
}

        Date dateDo = Date.valueOf(ldDateDo);
        Date dateOd = Date.valueOf(ldDateOd);
        int intKastrace = 0;
        if (this.getCheckBoxCastrated()) {
            intKastrace = 1;
        }
        Objednavka objednavka = new Objednavka(ID,dateOd, dateDo, objCena,
                                               this.getTextFieldOwnerName(),
                                               this.getTextFieldOwnerSurname(),
                                               this.getTextFieldDogName(),
                                               this.getTextFieldDogRace().
                                                       toString(), this.
                                                       getComboBoxDogSize().
                                                       toString(), this.
                                                       getComboBoxDogAge().
                                                       toString(),
                                              this.getComboBoxDogSex().
                                                       toString(), intKastrace);
        return objednavka;
    }

    /**
     * Metoda sloužící k načtení obrazovky Main.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadScrMain(ActionEvent event) throws IOException {
        //načte scrOrder.fxml a následně nastaví scénu na tuto obrazovku
        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                                                                     .getResource("scrOrder.fxml")));
    }

    /**
     * Metoda sloužící k načtení obrazovky EditOrder.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadScrEditOrder(ActionEvent event) throws IOException {
        //načte scrEditOrder.fxml a následně nastaví scénu na tuto obrazovku
        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                                                                     .getResource("scrEditOrder.fxml")));
    }

    public LocalDate getDatePickerFrom() {
        return datePickerFrom.getValue();
    }

    public LocalDate getDatePickerTo() {
        return datePickerTo.getValue();
    }

    public String getTextFieldOwnerName() {
        return textFieldOwnerName.getText();
    }

    public String getTextFieldDogName() {
        return textFieldDogName.getText();
    }

    public String getTextFieldDogRace() {
        return textFieldDogRace.getText();
    }

    public String getTextFieldOwnerSurname() {
        return textFieldOwnerSurname.getText();
    }

    public String getTextFieldAdditionalInfo() {
        return textFieldAdditionalInfo.getText();
    }

    public pohlaviPes getComboBoxDogSex() {
        return comboBoxDogSex.getSelectionModel().getSelectedItem();
    }

    public vekPes getComboBoxDogAge() {
        return comboBoxDogAge.getSelectionModel().getSelectedItem();
    }

    public String getTextFieldPrice() {
        return textFieldPrice.getText();
    }

    public boolean getCheckBoxCastrated() {
        return checkBoxCastrated.isSelected();
    }

    public velikostPes getComboBoxDogSize() {
        return comboBoxDogSize.getSelectionModel().getSelectedItem();
    }

        public void vypoctiCenu()
    {
        //vynulování ceny
        cena = 0;


        //inicializace hodnot při výpočtu
        int pocetDni = (int) ChronoUnit.DAYS.between(this.getDatePickerFrom(), this.getDatePickerTo());
        InitializeValues(pocetDni, this.getComboBoxDogSize().toString(), this.getComboBoxDogAge().toString());

        //cena za den pobytu je 50 Kč + extra poplatky, viz předchozí funkce
        cena += pocetDni * 50;

        if (this.getCheckBoxCastrated() == false) {
            cena = cena + 250;
        }
    }


    private void InitializeValues(int PocetDni, String velikostPes,
                                  String vekPes)
    {
        switch (velikostPes) {
            case "STREDNI":
                cena += 200 * (PocetDni / 7); //need to cast to int? ( (int)200* ... )
                break;

            case "VELKY":
                cena += 350 * (PocetDni / 7);
                break;

            default:
                break;
        }

        switch (vekPes) {
            case "STENE":
                cena += 50;
                break;

            case "DOSPELY":
                cena += 300;
                break;

            case "SENIOR":
                cena -= 100;
                break;

            default:
                break;
        }
    }
}
