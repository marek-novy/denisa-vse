package GUI;

import bin.Objednavka;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Hlavní třída zde pouze definuje metodu, která deleguje vlastní spuštění
 * aplikace na třídu používající zadané uživatelské rozhraní.
 *
 * @author Stanislav Chitov, Lucie Marková, Denisa Nová
 * @version 2019-SUMMER
 */
public class Main extends Application {

    /**
     * Instance třídy Stage pouřitá jako prvek UI.
     */
    public static Stage parentWindow;

    /**
     * Názvy odkazů na původní a současné okno uložené ve formě Stringu.
     */
    public static String previousWindow;
    public static String thisWindow;
    public static Objednavka currentObjednavka;


    /**
     * Metoda třídy spouštějící aplikaci na základě zadaných argumentů.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Metoda třídy spouštějící UI aplikace. Zároveň inicializuje výše uvedené
     * proměnné, které si "pamatují" předchozí okno pro návrat z obrazovky
     * nápovědy na původní obrazovku.
     *
     * @param stage instance třídy Stage
     * @throws java.lang.Exception Obecná instance třídy Exception
     */
    @Override
    public void start(Stage stage) throws Exception {

        //inicializace proměnných
        parentWindow = stage;

        /** předchozí a toto okno jsou na začátku aplikace prázdné, thisWindow
         * je taktéž prázdné a deklaruje se až po nastavení prvotní obrazovky.
         */

        previousWindow = "";
        thisWindow = "";

        //nastavení jména aplikace
        stage.setTitle("Aplikace Psí Hotel");

        //nastavení aplikace - nedá se resizovat
        stage.setResizable(false);

        //načtení FXML souboru a následné přiřazení do objektu Parent (node)
        Parent root = FXMLLoader.load(getClass().getResource("scrMain.fxml"));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    public static Objednavka getCurrentObjednavka() {
        return currentObjednavka;
    }

    public static void setCurrentObjednavka(Objednavka currentObjednavka) {
        Main.currentObjednavka = currentObjednavka;
    }

}
