package GUI;

import static GUI.Main.parentWindow;
import static GUI.Main.previousWindow;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * Controller třída pro obrazovku EditOrder.
 *
 * @author Stanislav Chitov, Lucie Marková, Denisa Nová
 * @version 2019-SUMMER
 */
public class ScrEditOrderController implements Initializable {

    /** Inicializace všech komponentů UI ze souboru FXML. */
    @FXML
    private DatePicker datePickerFrom;
    @FXML
    private DatePicker datePickerTo;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField textFieldOwnerName;
    @FXML
    private TextField textFieldDogName;
    @FXML
    private TextField textFieldDogRace;
    @FXML
    private TextField textFieldOwnerSurname;
    @FXML
    private TextField textFieldAdditionalInfo;
    @FXML
    private ComboBox<?> comboBoxDogSex;
    @FXML
    private ComboBox<?> comboBoxDogAge;
    @FXML
    private TextField textFieldPrice;
    @FXML
    private CheckBox checkBoxCastrated;
    @FXML
    private ComboBox<?> comboBoxDogSize;

    /**
     * Metoxa inicializující třídu controlleru.
     * 
     * @param url instance URL
     * @param rb  instance ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /** přiřadí previousWindow stringovou hodnotu jména předchozího okna. */
        previousWindow = parentWindow.getScene().getRoot().toString();
    }   
    
    /**
     * Metoda, která uživatele při pokusu o zavření okna pomocí alertu upozorní,
     * že provedené změny doposud nebyly uloženy.
     * 
     * @param event event
     * @throws IOException 
     */
    @FXML
    private void alertOnUnsaved(ActionEvent event) throws IOException {
        
        //vytvoření instance třídy Alert
        Alert alertUnsaved = new Alert(Alert.AlertType.WARNING,
            "Chcete zahodit neuložené změny?",
            ButtonType.YES, ButtonType.NO);
        alertUnsaved.setTitle("Varování");
        alertUnsaved.setHeaderText("Neuložené změny");
        
        //ošetření NullPointerException pomocí třídy Optional
        Optional<ButtonType> result = alertUnsaved.showAndWait();
        
        //pokud uživatel zvolí tlačítko ano, bude načtena původní obrazovka
        if (result.get() == ButtonType.YES)
            loadPrevScr(event);      
    }
    
    
    /**
     * Metoda informující uživatele o úspěšném uložení provedených změn.
     * 
     * @param event event
     * @throws IOException 
     */
    @FXML
    private void alertSaved(ActionEvent event) throws IOException {
        
        //vytvoření instance třídy Alert
        Alert alertFinished = new Alert(Alert.AlertType.INFORMATION);
        
        alertFinished.setTitle("Operace provedena");
        alertFinished.setHeaderText("");
        alertFinished.setContentText("Změny byly úspěšně uloženy.");
        alertFinished.showAndWait();
        
        //načtení původní obrazovky
        loadScrOrder(event);
    }
    
    /**
     * Metoda sloužící k načtení obrazovky Order.
     * 
     * @param event event
     * @throws IOException 
     */
    private void loadScrOrder(ActionEvent event) throws IOException {
        //načte scrOrder.fxml a následně nastaví scénu na tuto obrazovku
        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                .getResource("scrOrder.fxml")));
    }
    
    /**
     * Metoda sloužící k načtení původní obrazovky.
     * 
     * @param event event
     * @throws IOException 
     */
    private void loadPrevScr(ActionEvent event) throws IOException {
        
        //ověří, zda se předchozí obrazovka jménem rovná obrazovce scrOrder
        if (previousWindow.equals("VBox[id=vBoxScrOrder, styleClass=root]"))
            loadScrOrder(event);
        
        else 
            //načte scrOrderDetails.fxml a následně nastaví scénu na tuto obrazovku
            Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                .getResource("scrOrderDetails.fxml")));
    }
}
