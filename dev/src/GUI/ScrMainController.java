package GUI;

import static GUI.Main.parentWindow;
import static GUI.Main.previousWindow;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 * Controller třída pro obrazovku Main.
 *
 * @author Stanislav Chitov, Lucie Marková, Denisa Nová
 * @version 2019-SUMMER
 */
public class ScrMainController implements Initializable {

    /** Inicializace všech komponentů UI ze souboru FXML. */
    @FXML
    private MenuItem menuItemCreateOrder;
    @FXML
    private MenuItem menuItemEnd;
    @FXML
    private MenuItem menuItemAbout;
    @FXML
    private MenuItem menuItemHelp;
    @FXML
    private ImageView imgCreateOrder;
    @FXML
    private ImageView imgShowOrders;
    @FXML
    private AnchorPane rootPane;
    @FXML
    private VBox vBoxScrMain;

    /**
     * Metoxa inicializující třídu controlleru.
     *
     * @param url instance URL
     * @param rb  instance ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        /* pokud je obrazovka Main načtena poprvé (při spuštění aplikace),
           neexistuje předchozí okno a tudíž bude previousWindow v tomto
           okamžiku stále rovno "" (tj. prázdnémů stringu). */
        if (!previousWindow.equals(""))
            /** přiřadí previousWindow stringovou hodnotu jména
             * předchozího okna. */
            previousWindow = parentWindow.getScene().getRoot().toString();
    }

    /**
     * Metoda sloužící k načtení obrazovky CreateOrder.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadScrCreateOrder(MouseEvent event) throws IOException {
    //načte scrCreateOrder.fxml a následně nastaví scénu na tuto obrazovku
    Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
        .getResource("scrOrderDetails.fxml")));
    }

    /**
     * Metoda sloužící k načtení obrazovky Order.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadScrOrders(MouseEvent event) throws IOException {
    //načte scrOrder.fxml a následně nastaví scénu na tuto obrazovku
    Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
            .getResource("scrOrder.fxml")));
    }

    /**
     * Pomocná metoda sloužící k načtení obrazovky CreateOrder, která se spustí
     * pouze v případě použití klávesové zkratky; v případě kliknutí myši
     * se naopak použije metoda loadScrCreateOrder.
     *
     * @param event
     * @throws IOException
     */
    @FXML
    private void menuScrCreateOrder(ActionEvent event) throws IOException {
    //načte scrCreateOrder.fxml a následně nastaví scénu na tuto obrazovku
    Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
        .getResource("scrCreateOrder.fxml")));
    }

    /**
     * Metoda sloužící k ukončení aplikace.
     *
     * @param event
     */
    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metoda sloužící k zobrazení informací o programu prostřednictvím Alertu.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadAbout(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("O programu");
            alert.setHeaderText("Semestrální práce - Dog Hotel");
            alert.setContentText("verze LS 2019");
            alert.showAndWait();
    }

    /**
     * Metoda sloužící k načtení obrazovky Help.
     *
     * @param event event
     * @throws IOException
     */
    @FXML
    private void loadScrHelp(ActionEvent event) throws IOException {
        //načte scrHelp.fxml a následně nastaví scénu na tuto obrazovku
        Main.parentWindow.getScene().setRoot(FXMLLoader.load(getClass()
                .getResource("scrHelp.fxml")));
    }
}
